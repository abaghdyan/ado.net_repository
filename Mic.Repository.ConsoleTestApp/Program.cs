﻿using Mic.Repository;
using Mic.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Mic.Repository.ConsoleTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string conStr = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=Universities_DB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
            using (var dbContext = new DbContext(conStr))
            {
                try
                {
                    var professionRepo = new ProfessionRepository(dbContext);
                    Profession profession = new Profession { ProfessionName = "Physics" };
                    professionRepo.Insert(profession);

                    var genderRepo = new GenderRepository(dbContext);
                    genderRepo.Delete(1);

                    var universityRepo = new UniversityRepository(dbContext);
                    University University = new University { ShortName = "EPH", FullName = "Erevani Petakan Hamalsaran", AdressInYerevan = "Yerevan", RectorsName = "EPH_Rector", Phone = "+374374374" };
                    universityRepo.Insert(University);

                    var studentRepo = new StudentRepository(dbContext);
                    Student student = new Student { Name = "John", Surname = "Smith", Gender_Id = 1, University_Id = 1, Age = 18, Mark = 85 };
                    studentRepo.Insert(student);

                    var techerRepo = new TeacherRepository(dbContext);
                    Teacher teacher = new Teacher { Name = "Bill", Surname = "Gates", Gender_Id = 1, University_Id = 3, Age = 99, Profession_Id = 1 };
                    techerRepo.InsertOrUpdate(teacher);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            Console.WriteLine("All Done!");
            Console.ReadLine();
        }
    }
}
