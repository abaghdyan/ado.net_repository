﻿using Mic.Repository.Consts;
using Mic.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Mic.Repository
{
    public class ProfessionRepository : BaseRepository<Profession>
    {
        public ProfessionRepository(DbContext dbContext) : base(dbContext)
        { }

        public override string TableName => DbNames.Table_Profession;

        protected override Profession CreateEntity(IDataReader reader)
        {
            return new Profession
            {
                Id = (int)reader["Id"],
                ProfessionName = (string)reader["ProfessionName"],
            };
        }
    }
}
