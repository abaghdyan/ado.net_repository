﻿using Mic.Repository.Consts;
using Mic.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Mic.Repository
{
    public class TeacherRepository : BaseRepository<Teacher>
    {
        public TeacherRepository(DbContext dbContext) : base(dbContext)
        { }

        public override string TableName => DbNames.Table_Teacher;

        protected override Teacher CreateEntity(IDataReader reader)
        {
            return new Teacher
            {
                Id = (int)reader["Id"],
                Name = (string)reader["Name"],
                Surname = (string)reader["Surname"],
                Gender_Id = (int)reader["Gender_Id"],
                University_Id = (int)reader["University_Id"],
                Age = (byte)reader["Age"],
                Profession_Id = (int)reader["Profession"]
            };
        }
    }
}
