﻿using Mic.Repository.Consts;
using Mic.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Mic.Repository
{
    public class GenderRepository : BaseRepository<Gender>
    {
        public GenderRepository(DbContext dbContext) : base(dbContext)
        { }

        public override string TableName => DbNames.Table_Gender;

        protected override Gender CreateEntity(IDataReader reader)
        {
            return new Gender
            {
                Id = (int)reader["Id"],
                GenderName = (string)reader["GenderName"],
            };
        }
    }
}
