﻿namespace Mic.Repository.Consts
{
    public static class DbNames
    {
        public const string Table_Gender = "Gender";
        public const string Table_Profession = "Profession";
        public const string Table_Student = "Student";
        public const string Table_Teacher = "Teacher";
        public const string Table_University = "University";

        public const string Col_Id = "Id";
        public const string Col_GenderName = "GenderName";
        public const string Col_ProfessionName = "ProfessionName";
        public const string Col_Name = "Name";
        public const string Col_Surname = "Surname";
        public const string Col_Gender_Id = "Gender_Id";
        public const string Col_University_Id = "University_Id";
        public const string Col_Age = "Age";
        public const string Col_Mark = "Mark";
        public const string Col_Profession_Id = "Profession_Id";
        public const string Col_ShortName = "ShortName";
        public const string Col_FullName = "FullName";
        public const string Col_AdressInYerevan = "AdressInYerevan";
        public const string Col_Phone = "Phone";
        public const string Col_RectorsName = "RectorsName";
        public const string Col_DestroyDate = "DestroyDate";
    }
}