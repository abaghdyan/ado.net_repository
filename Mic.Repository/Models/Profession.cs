﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mic.Repository.Entities
{
    public class Profession
    {
        public int Id { get; set; }
        public string ProfessionName { get; set; }
    }
}
