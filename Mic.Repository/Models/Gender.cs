﻿namespace Mic.Repository.Entities
{
    public class Gender
    {
        public int Id { get; set; }
        public string GenderName { get; set; }
    }
}
