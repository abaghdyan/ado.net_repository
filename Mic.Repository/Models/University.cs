﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mic.Repository.Entities
{
    public class University 
    {
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string AdressInYerevan { get; set; }
        public string Phone { get; set; }
        public string RectorsName { get; set; }
        public DateTime? DestroyDate { get; set; }
    }
}
