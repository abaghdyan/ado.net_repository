﻿namespace Mic.Repository.Entities
{
    public class Teacher
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Gender_Id { get; set; }
        public int University_Id { get; set; }
        public byte Age { get; set; }
        public int Profession_Id { get; set; }
    }
}
