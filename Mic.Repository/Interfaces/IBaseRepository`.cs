﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mic.Repository
{
    public interface IBaseRepository<TEntity>
        where TEntity : class, new()
    {
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> GetAll(string query);
        TEntity GetById(int id);
        int Insert(TEntity entity);
        int Delete(int id);
        int Update(TEntity entity);
        int InsertOrUpdate(TEntity entity);
    }
}
