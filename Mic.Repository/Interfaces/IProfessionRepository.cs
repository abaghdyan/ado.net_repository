﻿using Mic.Repository.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mic.Repository
{
    public interface IProfessionRepository : IBaseRepository<Profession>
    {
    }
}
