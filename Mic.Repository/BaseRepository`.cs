﻿using Mic.Repository.Consts;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Text;

namespace Mic.Repository
{
    public abstract class BaseRepository<TEntity> : BaseRepository, IBaseRepository<TEntity>
        where TEntity : class, new()
    {
        protected BaseRepository(DbContext dbContext) : base(dbContext) { }
        protected abstract TEntity CreateEntity(IDataReader reader);

        public IEnumerable<TEntity> GetAll()
        {
            string query = string.Format(Queries.SelectAll, TableName);
            return OnExecute(query).Select(CreateEntity);
        }

        public IEnumerable<TEntity> GetAll(string query)
        {
            return OnExecute(query).Select(CreateEntity);
        }

        public TEntity GetById(int id)
        {
            string query = string.Format(Queries.SelectOne, TableName, PrimaryKey, id);
            return OnExecute(query).Select(CreateEntity).FirstOrDefault();
        }

        public int Delete(int id)
        {
            string query = string.Format(Queries.Delete, TableName, PrimaryKey, id);
            return OnExecuteNonQuery(query, null);
        }

        public int Insert(TEntity entity)
        {
            try
            {
                var nameBuilder = new StringBuilder();
                var valueBuilder = new StringBuilder();
                List<SqlParameter> parameters = new List<SqlParameter>();

                foreach (var prop in entity.GetType().GetProperties().Where(p => p.Name != PrimaryKey))
                {
                    nameBuilder.Append(prop.Name).Append(",");
                    valueBuilder.Append("@").Append(prop.Name).Append(",");

                    var sqlP = new SqlParameter(prop.Name, prop.PropertyType);
                    object value = prop.GetValue(entity);
                    if (value == null)
                        value = DBNull.Value;
                    sqlP.Value = value;
                    parameters.Add(sqlP);
                }
                string nameText = nameBuilder.ToString().TrimEnd(',');
                string valueText = valueBuilder.ToString().TrimEnd(',');
                string query = string.Format(Queries.InsertScalar, TableName, nameText, valueText);
                return (int)OnExecuteScalar(query, parameters);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        public int Update(TEntity entity)
        {
            try
            {
                var colNameValueText = new StringBuilder();
                List<SqlParameter> parameters = new List<SqlParameter>();
                var pKeyProp = entity.GetType().GetProperties().FirstOrDefault(p => p.Name == PrimaryKey);
                var pKeyPropVal = pKeyProp.GetValue(entity);

                TEntity selectedEntity = GetById((int)pKeyPropVal);

                foreach (var prop in entity.GetType().GetProperties().Where(p => p.Name != PrimaryKey))
                {
                    var test = selectedEntity.GetType().GetProperties().FirstOrDefault(p => p.Name == prop.Name);
                    if (test.GetValue(selectedEntity).ToString() != (string)prop.GetValue(entity).ToString())
                    {
                        object value = prop.GetValue(entity);
                        colNameValueText.Append(prop.Name).Append("=").Append("@").Append(prop.Name).Append(",");
                        var sqlP = new SqlParameter(prop.Name, prop.PropertyType);
                        if (value == null)
                            value = DBNull.Value;
                        sqlP.Value = value;
                        parameters.Add(sqlP);
                    }
                }

                string colNameValue = colNameValueText.ToString().TrimEnd(',');
                string query = string.Format(Queries.Update, TableName, colNameValue, PrimaryKey, pKeyPropVal);

                return OnExecuteNonQuery(query, parameters);

            }
            catch (Exception)
            {
                return -1;
            }

        }

        public int InsertOrUpdate(TEntity entity)
        {
            var pKeyProp = entity.GetType().GetProperties().FirstOrDefault(p => p.Name == PrimaryKey);
            var pKeyPropVal = pKeyProp?.GetValue(entity);
            TEntity selectedEntity = GetById((int)pKeyPropVal);

            if (selectedEntity == null)
            {
                return Insert(entity);
            }
            else
            {
                return Update(entity);
            }
        }

    }
}